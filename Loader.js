import React from 'react';
import { StyleSheet, Text, View } from 'react-native';


export default function() {
    return  (<View style={styles.container}>
                <Text style={styles.text}>Getting the fucking Weather</Text>
            </View>)
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FDF6AA",
        justifyContent: "flex-end"
    },
    text: {
        color: '#2c2c2c',
        fontSize: 30,
        paddingHorizontal: 30,
        paddingVertical: 100
    }
})