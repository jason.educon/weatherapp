import React from 'react';
import { StyleSheet, Text, View, Alert } from 'react-native';
import Loader from './Loader'
import * as Location from 'expo-location'
import axios from 'axios'
import Weather from './Weather'

const API_KEY = '10b98741849d959ce147146e3fc91c46'

export default class extends React.Component {

  state = {
    isLoading: true
  }

  getWeather = async (latitude, longitude) => {
    const {
      data: {
        main: {temp},
        weather
      }
    } = await axios.get(
      `http://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&APPID=${API_KEY}&units=metric`
    )
    this.setState({
      isLoading: false, 
      temp,
      condition: 'Clear'
    })
  }

  getLocation = async () => {
    try {
      await Location.requestPermissionsAsync()
      const{ coords:{latitude, longitude} } = await Location.getCurrentPositionAsync()
      this.getWeather(latitude, longitude)
    } catch (error) {
      Alert.alert("Can't find you")
    }
  }

  componentDidMount(){
    this.getLocation()
  }
  
  render(){
    const {isLoading, temp, condition} = this.state
    return (isLoading ? <Loader/> : <Weather temp={Math.round(temp)} condition={condition}/>)
  }
}
